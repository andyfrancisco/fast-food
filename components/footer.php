<footer class="footer">

   <section class="grid">

      <div class="box">
         <img src="images/email-icon.png" alt="">
         <h3>Mail</h3>
         <a href="mailto:franciscoenzo@icloud.com">franciscoenzo@icloud.com</a>
         <a href="mailto:jordychodaton@gmil.com">jordychodaton@gmail.com</a>
      </div>

      <div class="box">
         <img src="images/clock-icon.png" alt="">
         <h3>Horaire</h3>
         <p>11h-17h</p>
      </div>

      <div class="box">
         <img src="images/map-icon.png" alt="">
         <h3>Adress</h3>
         <a href="#">Technôpole du Madrillet</a>
      </div>

      <div class="box">
         <img src="images/phone-icon.png" alt="">
         <h3>Numero</h3>
         <a href="tel:111122222">111-122-222</a>
         <a href="tel:222333333">222-333-333</a>
      </div>

   </section>

   

</footer>

